<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	function pre_r($expression, $return = false){
		if ($return)
		{
		  if (is_string($expression)) return '<pre>' . print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), true) . '</pre>';
			return '<pre>' . print_r($expression, true) . '</pre>';
		}
		else
		{
			echo '<pre>';
			if (is_string($expression)) print_r(str_replace(array('<','>'), array('&lt;','&gt;'), $expression), false);
			else print_r($expression, false);
			echo '</pre>';
		}
	}

	function line_with_argument($lang,$variable){
        return str_replace('%s',$variable,$lang);
    }
    
    function line_with_arguments($lang,$variables = array()){
        $exploded = explode('%s',$lang);
    
        $str = '';
    
        foreach($exploded as $key => $explode){
            if(isset($variables[$key])){
                $str .= $explode.' '.$variables[$key].' ';
            }
            else{
                $str .= $explode.' ';
            }
        }
    
        return $str;
    }

	function generateToken($length = 10){
		$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString     = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}

	function __($key){
		return lang($key) ? lang($key) : $key;
	}

?>