<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Cortex {

    function __construct()
    {
        $this->CI = & get_instance();

        // Load the Sessions class
        $this->CI->load->driver('session');
    }

    function login($data)
    {
        $this->CI->curl->create('http://api.softsolutions.sk/loyalty_systems/cortex/login');
        //$this->CI->curl->http_header('API-KEY', $this->CI->themeConfig['apiKey']);
        $this->CI->curl->post($data);
        $response = json_decode($this->CI->curl->execute());
        $this->CI->load->helper('cookie');

        if (isset($_POST['remember'])){
            $user_id_cookie = array(
                'name'   => 'gps_user_id',
                'value'  => $response->data->idcont,
                'expire' => '315360000',  // 10 rokov
            );
            $token_cookie = array(
                'name'   => 'gps_token',
                'value'  => $response->data->user_token,
                'expire' => '315360000',  // 10 rokov
            );

            $remember_cookie = array(
                'name'   => 'gps_remember',
                'value'  => 1,
                'expire' => '315360000',  // 10 rokov
            );

            set_cookie($user_id_cookie);
            set_cookie($token_cookie);
            set_cookie($remember_cookie);
        }


        if (isset($response->status) && ($response->status == TRUE)) {
            $this->CI->session->set_userdata('loyault', $response->data);
            $this->getUserData();

            echo json_encode(array(
                'status' => 1
            ));
        }
        else
        {
            echo json_encode(array(
                'status' => 0
            ));
        }
    }

    function getUserData()
    {
        if (isset($_COOKIE['gps_remember']) && $_COOKIE['gps_remember'] == 1){
            if (isset($_COOKIE['gps_user_id']) && isset($_COOKIE['gps_token']) && ($_COOKIE['gps_remember']) == 1){
                $data = array(
                    'userToken' => $_COOKIE['gps_token'],
                    'userID' => $_COOKIE['gps_user_id'],
                );
            }
        }
        elseif ($this->CI->session->has_userdata('loyault')){
            $loyalty = $this->CI->session->userdata('loyault');
            if (isset($loyalty->user_token) && isset($loyalty->idcont)){
                $data = array(
                    'userToken' => $loyalty->user_token,
                    'userID' => $loyalty->idcont,
                );
            }
        }else{
            $data = 'error';
        }

        $this->CI->curl->create('http://api.softsolutions.sk/loyalty_systems/cortex/getUserInfo');
        //$this->CI->curl->http_header('API-KEY', $this->CI->themeConfig['apiKey']);
        $this->CI->curl->post($data);
        $response = json_decode($this->CI->curl->execute());

        //pre_r($data);exit;
        if (isset($response->status) && ($response->status == TRUE)) {
            $this->CI->session->set_userdata('loyault_user', $response->data);
            return $response->data;
        }else{
            return FALSE;
        }
    }

    function logout($data){

        $this->CI->load->helper('cookie');
        $this->CI->curl->create('https://api.softsolutions.sk/loyalty_systems/cortex/logout');
        //$this->CI->curl->http_header('API-KEY', $this->CI->themeConfig['apiKey']);
        $this->CI->curl->post($data);

        $this->CI->session->unset_userdata('loyault');
        $this->CI->session->unset_userdata('loyault_user');


        delete_cookie('gps_user_id');
        delete_cookie('gps_token');
        delete_cookie('gps_remember');

        $url=$this->CI->session->userdata('url');
        redirect($url, 'refresh');
    }
}
