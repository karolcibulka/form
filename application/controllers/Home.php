<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    private $post_data;
    private $errors;
    private $messages;
    private $required_fields;
    private $allowed_extension;

    public function __construct()
    {
        parent::__construct();
        $this->required_fields  = [ 'first_name' , 'last_name' , 'email' , 'password' , 'password_confirmation' ];
        $this->validate_methods = [ 'validateEmail' , 'validatePassword' ];
        $this->allowed_extension = [ 'png' , 'jpg' ];

        $this->load->model('User_model','user_model');
        $this->load->library('bcrypt');

    }

    public function index()
    {

        if($this->post_data = $this->input->post()){
            if($this->validate()){
                if($data['user_id'] = $this->storeUser()){
                    $this->messages[] = __('registration.success');
                }
            }

            $data['post_data'] = $this->post_data;
        }

        $data['errors'] = $this->errors;
        $data['messages'] = $this->messages;

        if(empty($this->errors) && $this->request){
            $data['response'] = $this->request;
        }

        $this->template->view('home',$data);
    }

    private function validate(){
        foreach($this->required_fields as $required_field){
            if(!isset($this->post_data[$required_field]) || empty($this->post_data[$required_field])){
                $this->errors[] = __('required.'.$required_field);
            }
        }

        foreach($this->post_data as $field => $item){
            if(!in_array($field,$this->required_fields)){
                unset($this->post_data[$field]);
            }
        }

        if(empty($this->errors)){
            foreach($this->validate_methods as $method){
                $this->$method();
            }
        }

        return empty($this->errors);
    }

    private function validateEmail(){
        if (!filter_var($this->post_data['email'], FILTER_VALIDATE_EMAIL)) {
           $this->errors[] = __('validation.email.format');
        }

        if($this->user_model->getUserByEmail($this->post_data['email'])){
            $this->errors[] = line_with_argument(__('validation.email.exist'),$this->post_data['email']);
        }
    }

    private function validatePassword(){
        if($this->post_data['password'] != $this->post_data['password_confirmation']){
            $this->errors[] = __('validation.password.match');
        }

        if(strlen($this->post_data['password']) < 8){
            $this->errors[] = __('validation.password.length');
        }
    }

    private function storeUser(){

        $salt = generateToken(20);

        $this->request = array(
            'first_name' => $this->post_data['first_name'],
            'last_name' => $this->post_data['last_name'],
            'email' => $this->post_data['email'],
            'password' => $this->bcrypt->hash_password($this->post_data['password'].$salt),
            'profile_image' => $this->getImage(),
            'salt' => $salt,
            'active' => 1,
            'deleted' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        );

        return $this->user_model->storeUser($this->request);
    }

    private function getImage(){

        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){

            $this->load->library('resize');

            $temp_name  = $_FILES['image']['name'];
            $extension  = strtolower(substr(strrchr($temp_name, '.'), 1));

            if(!in_array($extension,$this->allowed_extension)){
                return null;
            }

            $image_name = generateToken(30);

            if (!file_exists($this->upload_dir)) {
                mkdir($this->upload_dir, 0775, true);
            }

            $image = $image_name . '.' . $extension;

            copy($_FILES['image']['tmp_name'], $this->upload_dir . $image);

            $resized_image_name = $image_name. '-thumb.'.$extension;

            if(file_exists($this->upload_dir.$image)){
                $resizeObjThumb = new resize($this->upload_dir.$image);
                $resizeObjThumb->resizeImage(200, 0, 'landscape');
                $resizeObjThumb->saveImage($this->upload_dir. $resized_image_name, 100);

                unlink($this->upload_dir.$image);
            }

            return $resized_image_name;
        }

        return null;
    }
}