<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Template {

	private $theme = "default";
    private $layout = "views/layout/master";
	private $layout_data = array();
	private $ci;

	public function __construct()
	{
		$this->ci = & get_instance();
	}

	public function set($name, $value)
	{
		$this->layout_data[$name] = $value;
	}

	public function setTheme($theme){
		$this->theme = $theme;
	}

	public function setLayout($layout){
		$this->layout = 'views/layout/' . $layout;
	}


	public function load($template = '', $view = '', $view_data = array())
	{
		$this->set('content', $this->ci->load->view($view, $view_data, TRUE));

		return $this->load->view($template, $this->layout_data);
	}


	public function view($view = '', $view_data = array())
	{
		$this->set('content', $this->ci->load->view($this->theme . '/views/' . $view, array_merge($view_data, $this->layout_data) , TRUE));

		return $this->ci->load->view($this->theme .'/'. $this->layout, $this->layout_data);
	}

	public function getView($view = '', $view_data = array())
	{
		$view = str_replace('.php','',$view);
		
		if(file_exists(APPPATH.'../themes/'.$this->theme.'/views/'.$view.'.php')){
			return $this->ci->load->view($this->theme . '/views/' . $view, array_merge($view_data, $this->layout_data), TRUE);
		}
		return '';
	}

}