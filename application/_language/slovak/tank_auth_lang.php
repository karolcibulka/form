<?php
// Languages
$lang['auth_lang_slovak'] = 'Slovenčina';
$lang['auth_lang_czech'] = 'Čestina';
$lang['auth_lang_english'] = 'Angličtina';
// Errors
$lang['auth_incorrect_password'] = 'Nesprávne heslo';
$lang['auth_incorrect_login'] = 'Nesprávne prhlasovacie údaje!';
$lang['auth_incorrect_email_or_username'] = 'Login alebo email neexistuje';
$lang['auth_email_in_use'] = 'Zadaný email už používa iná osoba. Zvoľte si iný prosím.';
$lang['auth_username_in_use'] = 'Zadaný nick už používa iná osoba. Zvoľte si iný prosím.';
$lang['auth_current_email'] = 'Toto je tvoj súčastný email';
$lang['auth_incorrect_captcha'] = 'Potvrdzujúci kód sa nezhoduje s obrázkom. Skúste to znovu prosím.';
$lang['auth_captcha_expired'] = 'Potvrdzujúci kód stratil platnosť. Skúste to znovu prosím.';
$lang['auth_message_not_activated'] = 'Vaše konto ešte nebolo aktivované!';

// js error
$lang['auth_js_username_in_use'] = 'Toto Užívateľské meno sa poúžíva!';
$lang['auth_js_email_in_use'] = 'Tento email sa už používa!';
$lang['auth_js_email_not_in_use'] = 'Tento email nebol nájdený';
$lang['auth_js_min_length'] = 'Minimálne %s znakov!';
$lang['auth_js_no_spaces'] = 'Nesmie obsahovať medzery!';
$lang['auth_js_name'] = 'Vaše Meno a Priezvisko';;
$lang['auth_js_email_incorrect'] = 'Zadaný email nieje správny!';
$lang['auth_js_email_not_match'] = 'Emaily sa nezhodujú';
$lang['auth_js_pass_not_match'] = 'Heslá sa nezhodujú!';
//$lang['auth_js_username_in_use'] = '';

// form headlines
$lang['auth_login'] = 'Prihlásenie';
$lang['auth_registration'] = 'Registrácia';
$lang['auth_send_again'] = 'Preposlať aktivačný mail';
$lang['auth_forgot_password'] = 'Zabudnuté Heslo';
$lang['auth_send_invitaion'] = 'Poslať Pozvánku';
$lang['auth_reset_password'] = 'Obnova hesla';
$lang['auth_change_email'] = 'Zmena emailu';

// inputs
$lang['auth_input_placeholder_login'] = 'Email alebo Užívateľské meno';
$lang['auth_input_placeholder_password'] = 'Heslo';
$lang['auth_input_placeholder_confirm_password'] = 'Protvrďte Heslo';
$lang['auth_input_placeholder_email'] = 'Email';
$lang['auth_input_placeholder_confirm_email'] = 'Protvrďte Email';
$lang['auth_input_placeholder_name'] = 'Vaše meno';
$lang['auth_input_placeholder_surname'] = 'Priezvisko';
$lang['auth_input_placeholder_username'] = 'Užívateľské meno';
$lang['auth_input_placeholder_old_password'] = 'Staré heslo';
$lang['auth_input_placeholder_new_password'] = 'Nové heslo';
$lang['auth_input_placeholder_confirm_new_password'] = 'Potvrďte nové heslo';

// Buttons
$lang['auth_btn_login_register'] = 'Prihlásiť / Zaregistrovať';
$lang['auth_btn_forgot_password'] = 'Zabudol som heslo';
$lang['auth_btn_logout'] = 'Odhlasiť sa';
$lang['auth_btn_submit'] = 'Odoslať';
$lang['auth_btn_login'] = 'Prihlásiť';
$lang['auth_btn_register'] = 'Zaregistrovať sa';
$lang['auth_btn_remember'] = 'Neodhlasovať';
$lang['auth_btn_change'] = 'Zmeniť';

// Invitations
$lang['auth_invitation_send_success'] = 'Pozvánka bola úspešne odslaná';
$lang['auth_invitation_send_error'] = 'Pozvánku sa nepodarilo odoslať';
$lang['auth_invitation_send_no_email'] = 'Nebol zadaný email, alebo email je nesprávny';

// Notifications
$lang['auth_success_loggged_in'] = 'Boli ste úspešne prihlásený!';
$lang['auth_message_logged_out'] = 'Boli ste úspešne odhlásený.';
$lang['auth_message_need_to_be_logged'] = 'Pre vstup na tieto stránky musíš byť prihlásený.';
$lang['auth_message_registration_disabled'] = 'Registrácia je vypnutá';
$lang['auth_message_registration_completed_1'] = 'Boli ste úspešne zaregistrovaný. Na vašu emailovú adresu bol odoslaný aktivačný email';
$lang['auth_message_registration_failed_1'] = 'Boli ste úspešne zaregistrovaný, ale nepodarilo sa Vám doslať aktivačný email';
$lang['auth_message_registration_completed_2'] = 'Boli ste úspešne zaregistrovaný.';
$lang['auth_message_activation_email_sent'] = 'Nová aktivačný mail bol poslaný na %s. Pokračujte podľa inštrukcií v emaili.';
$lang['auth_message_activation_completed'] = 'Váš účet bol úspešne aktivovaný, možete sa prihlásiť';
$lang['auth_message_activation_failed'] = 'Aktivačný kód, ktorý ste zadali je nesprávny, alebo expirovaný.';
$lang['auth_message_password_changed'] = 'Vaše heslo bolo úspešne zmenené';
$lang['auth_message_new_password_sent'] = 'Email s inštrukciami pre zmenu hesla Vám bol zaslaný.';
$lang['auth_message_new_password_activated'] = 'Heslo bolo úspešne obnovené';
$lang['auth_message_new_password_failed'] = 'Aktivačný kód, ktorý ste zadali je nesprávny, alebo expirovaný. Prosím zkontrolujte váš email a pokračujte podľa inštrukcií.';
$lang['auth_message_new_email_sent'] = 'Potvrdzovací email bol odoslaný na Vami zadanú adresu. Pokračujte podľa inštukcií v maili.';
$lang['auth_message_new_email_activated'] = 'Zmena emailu bol úspešná';
$lang['auth_message_new_email_failed'] = 'Váš aktivačný kód je nesprávny alebo je po expirácii. Prosím zkontrolujte váš email a pokračujte podľa inštrukcií.';
$lang['auth_message_banned'] = 'Váš účet je zablokovaný.';
$lang['auth_message_unregistered'] = 'Váš účet bol odstránený...';
$lang['auth_message_password_activated'] = 'Heslo bolo zmenené';
$lang['auth_message_password_failed'] = 'Heslo sa nepodarilo zmeniť!';
$lang['max_login_attempts_exceeded'] = 'Maximálny počet pokusov bol prekročený!';

// Email subjects
$lang['auth_subject_welcome'] = 'Vitajte v systéme %s!';
$lang['auth_subject_activate'] = 'Vitajte v systéme %s!';
$lang['auth_subject_forgot_password'] = 'Obnova hesla na %s';
$lang['auth_subject_reset_password'] = 'Zmena prihl. hesla na %s';
$lang['auth_subject_change_email'] = 'Zmena prihl. emailovej adresy na %s';
$lang['auth_subject_change_username'] = 'Zmena prihl. mena na %s';
$lang['auth_subject_invitation'] = 'Pozvánka do %s';

// forms
$lang['reg.username'] = 'Užívateľské meno';
$lang['reg.email'] = 'Email';
$lang['reg.pass'] = 'Heslo';
$lang['reg.confPass'] = 'Podtvrďte heslo';
$lang['check.remeber_me'] = 'Neodhlasovať ma';



/* End of file tank_auth_lang.php */
/* Location: ./application/language/english/tank_auth_lang.php 
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';*/