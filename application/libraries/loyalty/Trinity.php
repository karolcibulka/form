<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Trinity {

    private $bearerToken = 'C57FLfX7X2KR1zKmdof2';
    private $ci;

    function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->driver('session');
    }

    function preregistration($data){
        $this->ci->curl->create('https://api.trinityhotels.sk/api/v1/authUser?email='.$data['email'].'&password='.$data['password']);
        $this->ci->curl->http_header('Authorization', 'Bearer ' . $this->bearerToken);
        $response = json_decode($this->ci->curl->execute());

        if (isset($response->status) && ($response->status == 'ok')) {
            return (array)$response->data;
        }

        return array();
    }

    function login($data)
    {
        $this->ci->curl->create('https://api.trinityhotels.sk/api/v1/authUser?email='.$data['email'].'&password='.$data['password']);
        $this->ci->curl->http_header('Authorization', 'Bearer ' . $this->bearerToken);
        $response = json_decode($this->ci->curl->execute());

        if (isset($response->status) && ($response->status == 'ok')) {
            $this->ci->session->set_userdata('loyault', $response->data);
            $this->ci->session->set_userdata('loyault_token_trinity', $response->token);
            $this->ci->session->set_userdata('loyault_user', (object) array(
                'idcont' => '1',
                'cardnumber' => $response->data->cardNumber,
                'firstname' => $response->data->firstName,
                'lastname' => $response->data->lastName,
                'loyaltyDiscount' => $response->data->cardDiscount,
                'country' => $response->data->state,
                'email' => $response->data->email,
                'phone' => $response->data->phone,
                'address' => $response->data->street,
                'city' => $response->data->city,
                'zip' => $response->data->zip
            ));

            echo json_encode(array(
                'status' => 1,
                'showOffers' => 1
            ));
        }
        else
        {
            echo json_encode(array(
                'status' => 0,
                'showOffers' => 0
            ));
        }
    }

    function verifyToken($token)
    {
        $this->ci->curl->create('https://api.trinityhotels.sk/api/v1/verifyToken?token='.$token);
        $this->ci->curl->http_header('Authorization', 'Bearer ' . $this->bearerToken);
        $response = json_decode($this->ci->curl->execute());

        if (isset($response->status) && ($response->status == 'ok')) {
            $this->ci->session->set_userdata('loyault', $response->data);
            $this->ci->session->set_userdata('loyault_token_trinity', $response->token);
            $this->ci->session->set_userdata('loyault_user', (object) array(
                'idcont' => '1',
                'cardnumber' => $response->data->cardNumber,
                'firstname' => $response->data->firstName,
                'lastname' => $response->data->lastName,
                'loyaltyDiscount' => $response->data->cardDiscount,
                'country' => $response->data->state,
                'email' => $response->data->email,
                'phone' => $response->data->phone,
                'address' => $response->data->street,
                'city' => $response->data->city,
                'zip' => $response->data->zip
            ));

        }
    }

    function getUserData()
    {
        if ($this->ci->session->has_userdata('loyault_user'))
        {
            return $this->ci->session->userdata('loyault_user');
        }

        return FALSE;
    }



    function logout(){
        $this->ci->session->unset_userdata('loyault');
        $this->ci->session->unset_userdata('loyault_user');
        $this->ci->session->unset_userdata('loyault_token_trinity');


        $url=$this->ci->session->userdata('url');
        redirect($url, 'refresh');
    }
}
