<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['theme']['version'] = 'default';
$config['theme']['analytics'] = 'default';
$config['theme']['body'] = '';
$config['theme']['header'] = 'default';
$config['theme']['footer'] = 'default';
$config['theme']['customLang'] = 'tmr';
$config['theme']['enableAnalytics'] = FALSE;
$config['theme']['analyticsVersion'] = 'event';
$config['theme']['showFooterOnHomePage'] = FALSE;
$config['theme']['showHotelLogoInQuickbooker'] = TRUE;
$config['theme']['hotelID'] = 25;
$config['theme']['loyaltyAgencyID'] = 4;
$config['theme']['apiKey'] = 'JksU6wl5doo5qrWMi5gnifoHBeTUHO';
$config['theme']['webServiceEndPoint'] = 'https://api.softsolutions.sk';

$config['theme']['coloredPackage'] = true;
$config['theme']['storePopupCookie'] = true;

$config['theme']['externalLoyaltySystem'] = true;
$config['theme']['loyaltyApiEndpoint'] = 'http://newloyalty.softsolutions.sk';
$config['theme']['loyaltyApiKey'] = 'IXRZkaIvTvLVoO8JLetmcpu0VR6v03wB0sMyCbvL3hz4hH1JmsiMQDk0iLiEGllr';

$config['theme']['loyaltyModal'] = array(
    'active' => TRUE,
    'registrationUrl' => 'https://api.trinityhotels.sk/trinityclub/decider',
    'loyaltyImageUrl' => 'https://www.trinityhotels.sk/images/trinity-karta.jpg'
);
