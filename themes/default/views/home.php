<div class="container h-100vh">
    <div class="row mh-100">
        <div class="col-md-6 mx-auto my-auto">
            <div class="card br-0">
                <form action="<?=base_url($current_lang.'/home')?>" method="post" enctype="multipart/form-data">
                    <div class="card-body">
                        <h5 class="text-center fw-300"><?=__('content.registration_form')?></h5>
                        <?php if(isset($errors) && !empty($errors)):?>
                            <ul class="alert alert-danger">
                                <?php foreach($errors as $error):?>
                                    <li><?=$error?></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>
                        <?php if(isset($response) && !empty($response)):?>
                            <ul class="alert alert-primary alert-json">
                                <li><?=json_encode($response,JSON_PRETTY_PRINT)?></li>
                        </ul>
                        <?php endif;?>
                        <?php if(isset($messages) && !empty($messages)):?>
                            <ul class="alert alert-success">
                                <?php foreach($messages as $message):?>
                                    <li><?=$message?></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>
                        <hr>
                        <div class="form-group">
                            <label><?=__('content.first_name')?></label>
                            <input type="text" name="first_name" value="<?=isset($post_data['first_name']) ? $post_data['first_name'] : ''?>" data-registration-form placeholder="<?=__('content.first_name')?>" <?=!isset($post_data['first_name']) || empty($post_data['first_name']) ? 'readonly' : '' ?> class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?=__('content.last_name')?></label>
                            <input type="text" name="last_name" value="<?=isset($post_data['last_name']) ? $post_data['last_name'] : ''?>" data-registration-form placeholder="<?=__('content.last_name')?>" <?=!isset($post_data['first_name']) || empty($post_data['first_name']) ? 'readonly' : '' ?> class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?=__('content.email')?></label>
                            <input type="email" name="email" value="<?=isset($post_data['email']) ? $post_data['email'] : ''?>" data-registration-form placeholder="<?=__('content.email')?>" <?=!isset($post_data['first_name']) || empty($post_data['first_name']) ? 'readonly' : '' ?> class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?=__('content.password')?></label>
                            <input type="password" name="password"  data-registration-form placeholder="<?=__('content.password')?>" readonly class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?=__('content.password_confirmation')?></label>
                            <input type="password" name="password_confirmation"  data-registration-form placeholder="<?=__('content.password_confirmation')?>" readonly class="form-control">
                        </div>

                        <div class="form-group">
                            <label><?=__('content.profile_image')?></label>
                            <input type="file" class="form-control" name="image">
                        </div>

                        <hr>
                        <button class="btn btn-primary w-100">
                            <?=__('content.send_button')?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('focus','[data-registration-form]',function(e){
        e.preventDefault();
        e.stopPropagation();

        var _self = $(this);

        _self.removeAttr('readonly');
    });
</script>