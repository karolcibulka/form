<?php

$lang['main.findYourStay'] = 'Vyhľadajte si svoj pobyt';

$lang['quickbooker.checkIn'] = 'Príchod';
$lang['quickbooker.checkOut'] = 'Odchod';
$lang['quickbooker.promocode'] = 'Promokód';
$lang['quickbooker.adults'] = 'Dospelí';
$lang['quickbooker.childsAge'] = 'Vek detí';
$lang['quickbooker.search'] = 'Vyhľadať';


?>