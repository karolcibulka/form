<?php


class Traveldata
{

	public $ci;
	public $endpoint;
	public $apiKey;
	public $webServiceEndpoint;
	public $currentLang;
	public $hotelData;

	public function __construct()
	{
		$this->ci = & get_instance();
		//$this->ci->load->driver('session');
		$this->ci->config->load('theme');
		$this->ci->load->library('ReservationBasket',array(),'reservation_basket');

        $this->webServiceEndpoint = $this->ci->load->get_var('webServiceEndPoint');
        $this->currentLang  = $this->ci->load->get_var('currentLang');

        $this->hotelData = $this->ci->cache->get('hotelData_'.$this->currentLang);

		if(isset($this->hotelData['loyalty_endpoint'],$this->hotelData['loyalty_apikey']) && !empty($this->hotelData['loyalty_endpoint']) && !empty($this->hotelData['loyalty_apikey'])) {
			$this->endpoint = $this->hotelData['loyalty_endpoint'];
			$this->apikey = $this->hotelData['loyalty_apikey'];
		}
		else{
			die ('Missing params api-key,end-point');
		}

	}

	public function login($data)
	{
		$this->ci->curl->create($this->endpoint.'api/login');
		$this->ci->curl->http_header('X-Authorization',$this->apikey);
		$this->ci->curl->post($data);
		$response = json_decode($this->ci->curl->execute());

		$this->ci->load->helper('cookie');

		if (isset($_POST['remember'])){
			$user_id_cookie = array(
				'name'   => 'gps_user_id',
				'value'  => $response->data->idcont,
				'expire' => '315360000',  // 10 rokov
			);
			$token_cookie = array(
				'name'   => 'gps_token',
				'value'  => $response->data->user_token,
				'expire' => '315360000',  // 10 rokov
			);

			$remember_cookie = array(
				'name'   => 'gps_remember',
				'value'  => 1,
				'expire' => '315360000',  // 10 rokov
			);

			set_cookie($user_id_cookie);
			set_cookie($token_cookie);
			set_cookie($remember_cookie);
		}

		if (isset($response->status) && ($response->status == TRUE)) {
			$this->ci->session->set_userdata('loyault', $response->data);
			$this->getUserData();

			echo json_encode(array(
				'status' => 1
			));
		}
		else
		{
			echo json_encode(array(
				'status' => 0
			));
		}
	}

	public function getUserData()
	{
	    $data = array();

		if (isset($_COOKIE['gps_remember']) && $_COOKIE['gps_remember'] == 1) {
			if (isset($_COOKIE['gps_user_id']) && isset($_COOKIE['gps_token']) && ($_COOKIE['gps_remember']) == 1) {
				$data = array(
					'user_token' => $_COOKIE['gps_token'],
					'user_id' => $_COOKIE['gps_user_id'],
				);
			}
		} elseif ($this->ci->session->has_userdata('loyault')) {
			$loyalty = $this->ci->session->userdata('loyault');
			if (isset($loyalty->user_token) && isset($loyalty->idcont)) {
				$data = array(
					'user_token' => $loyalty->user_token,
					'user_id' => $loyalty->idcont,
				);
			}
		} else {
			$data = 'error';
		}

        $this->ci->curl->create($this->endpoint . 'api/getUserData');
        $this->ci->curl->http_header('X-Authorization', $this->apikey);
        $this->ci->curl->post($data);
        $response = json_decode($this->ci->curl->execute());

        if (isset($response->status) && ($response->status == TRUE)) {

            $response->data->refreshed_at = date('Y-m-d H:i:s');
            $this->ci->session->set_userdata('loyault_user', $response->data);

            return $response->data;
        }
        else {
            return FALSE;
        }
	}

	public function register($data){
        $this->ci->curl->create($this->hotelData['loyalty_endpoint'] . 'api/register');
        $this->ci->curl->http_header('X-Authorization', $this->hotelData['loyalty_apikey']);
        $this->ci->curl->post($data);
        $response = json_decode($this->ci->curl->execute(), true);

        if (isset($response['status']) && $response['status'] == true) {
            echo json_encode(array('status' => 0, 'login' => 2, 'message' => $response['message']));
        } else {
            echo json_encode(array('status' => 0, 'login' => 0, 'message' => $response['message']));
        }
    }

	function logout($data){

		$this->ci->load->helper('cookie');
		$this->ci->curl->create($this->endpoint.'api/logout');
		$this->ci->curl->http_header('X-Authorization', $this->apiKey);
		$this->ci->curl->post($data);

		$this->ci->session->unset_userdata('loyault');
		$this->ci->session->unset_userdata('loyault_user');
		$this->ci->session->unset_userdata('guestReservations');
        $this->ci->session->unset_userdata('guestAccommodations');
        $this->ci->session->unset_userdata('reservation_basket');

		delete_cookie('gps_user_id');
		delete_cookie('gps_token');
		delete_cookie('gps_remember');

		$url=$this->ci->session->userdata('url');
		return redirect($url, 'refresh');
	}
}
