<?php

class Titan
{

    protected $ci;
    protected $hotel_data;
    protected $apiKey;
    protected $hotelID;
    protected $webServiceEndpoint;
    protected $theme_config;
    protected $currentLang;

    //new version !!!
    protected $current_lang;
    protected $api_key;
    protected $api_endpoint;
    protected $loyalty_id;
    protected $user_data = array();
    protected $user = array();
    protected $web_service_endpoint;

    protected $messages;

    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->library('curl');
        $this->ci->load->library('session');
        $this->ci->load->library('template');
        $this->ci->load->library('TitanBasket',false,'TitanBasket');

        $this->ci->config->load('theme');
        $this->apiKey  = $this->ci->config->item('theme')['apiKey'];
        $this->hotelID = $this->ci->config->item('theme')['hotelID'];
        $this->web_service_endpoint  = $this->ci->load->get_var('webServiceEndPoint');
        $this->current_lang  = $this->ci->load->get_var('currentLang');

        $this->ci->load->config('theme');
        $this->theme_config = $this->ci->config->item('theme');
        $this->user_data = TitanBasket::getUserData();
        $this->user = TitanBasket::getUser();
        $this->hotel_data = $this->ci->cache->get('hotelData_'.$this->current_lang);

        $this->api_endpoint = $this->theme_config['titan']['endpoint'];
        $this->api_key = $this->theme_config['titan']['api_key'];
        $this->loyalty_id = $this->theme_config['titan']['loyalty_id'];

        if(isset($_GET['deleteData'])){
            TitanBasket::cleanData();
        }
    }

    public function login($data){

        if ($this->user_data = $this->callTitanAPI('hotel/users/login/'.$this->loyalty_id.'/'.$this->current_lang,$data)) {
            $this->setCookie($this->user_data);
            TitanBasket::setUserData($this->user_data);

            if ($guest = $this->getUserData()) {
                echo json_encode(array('status' => 1, 'guest' => $guest, 'layout' => $this->ci->template->getView('layout/partials/' . $this->theme_config['version'] . '/guest/logged', array('user' => $guest))));
                exit();
            }
        }
        else{
            if(isset($this->theme_config['titan']['preregistration']) && !empty($this->theme_config['titan']['preregistration'])){
                switch($this->theme_config['titan']['preregistration']){
                    case 'trinity':
                        $this->ci->load->library('loyalty/Trinity',array(),'trinity');
                        if($user_data = $this->ci->trinity->preregistration($data)){
                            $this->trinityPreregistration($user_data,$data);
                        }
                }

            }
        }

        echo json_encode(array('status' => 0, 'login' => 1, 'message' => is_array($this->messages) ? implode('<br>',$this->messages) : $this->messages));
    }

    public function logout(){
        TitanBasket::logout();

        $this->ci->load->library('Basket');
        $this->ci->basket->removeCredit();
        $this->ci->basket->removeCompensationVoucher();
        TitanBasket::removeSeasonTickets();

        redirect(TitanBasket::getUrl(), 'refresh');
    }

    public function verifyToken($session_token){
        if($response['data'] = $this->callTitanAPI('hotel/common/verifyToken/'.$this->loyalty_id.'/'.$session_token)){

            $this->user_data = array(
                'user_id' => isset($response['data']['id']) && !empty($response['data']['id']) ? $response['data']['id'] : '',
                'user_token' => isset($response['data']['token']) && !empty($response['data']['token']) ? $response['data']['token'] : ''
            );

            if(TitanBasket::compareAccounts($this->user_data)){
                TitanBasket::logout();
                $this->setCookie($this->user_data);
                TitanBasket::setUserData($this->user_data);
            }
        }
    }

    public function register($data){
        $response = array(
            'status' => 0,
            'login' => 0,
            'message' => ''
        );


        //pre_r($data);exit;
        if($api_response = $this->callTitanAPI('hotel/users/createUser/'.$this->loyalty_id.'/'.$this->current_lang,TitanBasket::getCreateUserData($data))){
            $response['login'] = 2;
        }
        else{
            $response['message'] = is_array($this->messages) ? implode('<br>',$this->messages) : $this->messages;
        }


        echo json_encode($response);
    }

    public function getUserData(){

        $this->loadData();

        if(!empty($this->user_data) && empty($this->user)){

            if($response['data'] = $this->callTitanAPI('hotel/users/getUserData/'.$this->loyalty_id.'/'.$this->current_lang,$this->user_data)){
                $this->user =  array(
                    'idcont' => $response['data']['id'],
                    'cardnumber' => '',
                    'external_id' => $response['data']['id'],
                    'firstname' => $response['data']['first_name'],
                    'lastname' => $response['data']['last_name'],
                    'country' => $response['data']['country'],
                    'email' => $response['data']['email'],
                    'phone' => $response['data']['phone'],
                    'address' => $response['data']['street'].' '.$response['data']['street_number'],
                    'street' => $response['data']['street'],
                    'street_number' => $response['data']['street_number'],
                    'city' => $response['data']['city'],
                    'zip' => $response['data']['zip'],
                    'loyaltyDiscount' => isset($response['data']['loyalty_discount']) && !empty($response['data']['loyalty_discount']) ? $response['data']['loyalty_discount'] : 0,
                    'loyaltyProfileName' => isset($response['data']['loyalty_profile_name']) && !empty($response['data']['loyalty_profile_name']) ? $response['data']['loyalty_profile_name'] : null,
                    'loyaltyProfile_id' => isset($response['data']['loyalty_profile_id']) && !empty($response['data']['loyalty_profile_id']) ? $response['data']['loyalty_profile_id'] : null,
                    'loyaltyProfile_code' => isset($response['data']['loyalty_profile_code']) && !empty($response['data']['loyalty_profile_code']) ? $response['data']['loyalty_profile_code'] : null,
                    'register_date' => $response['data']['created_at'],
                    'profile_picture' => $response['data']['profile_picture'],
                    'use_credit_wallet' => isset($response['data']['use_credit_wallet']) && !empty($response['data']['use_credit_wallet']) ? true : false,
                    'card' => isset($response['data']['card']) && !empty($response['data']['card']) ? $response['data']['card'] : null
                );
            }
        }

        if(!empty($this->user)){
            if($wallet['data'] = $this->callTitanAPI('hotel/wallet/getWallet/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token'])){
                $this->user['points'] = isset($wallet['data']['balance']) ? $wallet['data']['balance'] : 0;
                $this->user['credit'] = isset($wallet['data']['credit_balance']) ? $wallet['data']['credit_balance'] : 0;
            }
        }

        TitanBasket::setUser($this->user);

        return (object)$this->user;
    }

    public function vouchers($return_data = false){

        $all_season_tickets = array();

        if(!$all_season_tickets = TitanBasket::getSeasonTickets()){
            if($season_tickets = $this->callTitanApi('hotel/tickets/checkSeasonTicket/'.$this->loyalty_id,array('user_token'=>$this->user_data['user_token']))){
                if(isset($season_tickets['seasonTickets']) && !empty($season_tickets['seasonTickets'])){
                    foreach($season_tickets['seasonTickets'] as $key => $season_ticket){
                        if($response = $this->callTitanApi('hotel/tickets/checkVoucher/'.$this->loyalty_id,array('user_token'=>$this->user_data['user_token'],'ticket_number'=>$season_ticket['seasonTicketNumber']))){
                            if(isset($response['voucherList']) && !empty($response['voucherList'])){
                                $all_season_tickets[$key] = reset($response['voucherList']);
                            }
                        }
                    }
                }

                TitanBasket::storeSeasonTickets($all_season_tickets);
            }
        }

        //pre_r($all_season_tickets);exit;

        $data = array(
            'season_vouchers' => $this->formatTickets($all_season_tickets)
        );

        if($return_data){
            if(!isset($data['season_vouchers']) || empty($data['season_vouchers'])){
                unset($data['season_vouchers']);
            }

            if(!isset($data['vouchers']) || empty($data['vouchers'])){
                unset($data['vouchers']);
            }

            if(!empty($data)){
                return $data;
            }
            
            return array();
        }

        $this->__view('vouchers',$data);
    }

    private function formatTickets($tickets = array()){
        $response = array();

        if(!empty($tickets)){
            foreach($tickets as $key => $ticket){
                if(isset($ticket['canUse']) && !empty($ticket['canUse'])){
                    $response[$key] = array(
                        'name' => $ticket['voucherTypeName'].'<br><small>'.$ticket['event']['eventName'].'</small>',
                        'raw_name' => $ticket['voucherTypeName'],
                        'raw_event_name' => $ticket['event']['eventName'],
                        'value' => $ticket['price'] < $ticket['voucherDiscountAmount'] ? $ticket['voucherDiscountAmount'] : $ticket['price'],
                        'number' => $ticket['voucherNumber'],
                        'activated_from' => date('d.m.Y H:i:s',($ticket['activatedFrom']/1000)),
                        'color' => '#'.random_color()
                    );
                }
            }
        }

        return $response;
    }

    public function settings($data){

        if($countries = $this->callTravelDataAPI('booking/Master/getCountries/' . $this->current_lang)){
            $this->ci->template->set('countries', $countries);
        }

        if($post = $this->ci->input->post()){

            $post['profile_picture'] = $post['profile_image_base_64'];

            $image_validated = true;

            if(isset($post['profile_picture']) && !empty($post['profile_picture'])){
                if(getBase64ImageSize($post['profile_picture']) > 3){
                    $image_validated = false;
                }
            }

            if($image_validated){
                $this->ci->curl->create($this->api_endpoint.'hotel/users/changeUserData/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token']);
                $this->ci->curl->http_header('API-KEY', $this->api_key);
                $this->ci->curl->post($post);
                $response = json_decode($this->ci->curl->execute(),true);


                if(isset($response['data']) && !empty($response['data'])){
                    $this->user = null;
                    $this->ci->template->set('registeredGuestData',$this->getUserData());
                }
                if(isset($response['message']) && !empty($response['message'])){
                    $this->ci->template->set('errors',$response['message']);
                }
            }
            else{
                $data['errors'][] = 'Veľkosť obrázka musí byt menšia ako 3 mb';
            }
        }


        $this->__view('settings',$data);
    }

    public function walletHistory($type,$offset){

        //if(!$activities = TitanBasket::getWalletActivities($type)){
            if($activities = $this->callTitanAPI('hotel/wallet/getActivities/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token'].'/'.$type)){
                //TitanBasket::setWalletActivities($type,$activities);
            }
        //}

        //odkomentovat a dorobil ukladanie zaznamov s logickym premazavanim dat

        $paginate = $this->paginate($activities['activities'],false,'walletHistory/'.$type,false);

        $data = array(
            'histories' => $this->getRecords($paginate['records'], $offset, 10),
            'type' => $type,
            'currency' => $type == 'credit' ? $this->hotel_data['currencySymbol'] : lang('content.points')
        );

        //pre_r($data);exit;

        $this->__view('points',$data);
    }

    public function history($all){

        $type = 'history';

        if(!$guest_reservations = TitanBasket::getReservations()){
            if($guest_reservations = $this->callTitanAPI('hotel/booking/getBookings/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token'])){
                TitanBasket::setReservations($guest_reservations);
            }
        }

        $paginate = $this->paginate($guest_reservations,$all,$type);

        $data = array(
            'reservations' => $this->getRecords($paginate['records'],$paginate['offset'],$paginate['per_page']),
            'type' => 'booking',
            'all' => $all,
        );

        $this->__view('history',$data);

    }


    public function myReservation($reservationID){

        if(!$reservationID){
            redirect(base_url($this->current_lang . '/loyaltySystem/history'));
        }

        if (!$reservation = TitanBasket::getReservationArray($reservationID)) {
            redirect(base_url($this->current_lang . '/loyaltySystem/history'));
        }

        TitanBasket::removeOffersByReservationID($reservationID);

        $reservation['bookingData'] = array();
        $branch_hotel_data          = array();
        $upsells                    = array();
        $reservationData            = array();

        //pre_r($reservation);exit;
        if (isset($reservation['branch_name']) && !empty($reservation['branch_name'])) {

            if(!$branch_hotel_data = TitanBasket::getBranchHotelData($reservation['branch_name'])){
                $branch_hotel_data = $this->getBranchHotelData($reservation['branch_name']);
            }

            //pre_r($branch_hotel_data);exit;

            TitanBasket::setBranchHotelDataReservation($reservationID,$branch_hotel_data);


            if (!empty($branch_hotel_data)) {
                if(!$reservation['bookingData'] = TitanBasket::getBooking($reservationID)){
                    if($reservation['bookingData'] = $this->callTravelDataAPI('guests/services/getBooking2/' . $branch_hotel_data['hotelID'] . '/' . $reservation['order_id'])){
                        TitanBasket::setBooking($reservationID,$reservation['bookingData']);
                    }
                }
                

                if (!$upsells = TitanBasket::getUpsells($reservationID)) {
                   
                    if($upsells = $this->callTravelDataAPI('booking/hotel/getUpsellOffersLoyalty/' . $branch_hotel_data['hotelID'] . '/' . $reservationID)){
                        TitanBasket::setUpsells($reservationID, $upsells);
                    }
                }

                if(!$reservationData = TitanBasket::getUserReservation($reservationID)){
                    $reservationData = $this->getReservationDataTitan($branch_hotel_data['hotelID'],$reservationID);
                }
            }
        }

        $data = array(
            'reservation' => $reservation,
            'reservationID' => $reservationID,
            'upsells' => $upsells,
            'bookingStatus' => array(),
            'reservationData' => $reservationData,
            'reservationBasket' => TitanBasket::getBasket($reservationID),
            'remakedBasket' => TitanBasket::getRemakedBasket($reservationID),
            'branch_hotel_data' => $branch_hotel_data
        );

        $this->__view('myReservation',$data);
    }




    public function accommodations($all){

        $type = 'accommodation';

        if(!$guest_accommodations = TitanBasket::getAccommodations()){
            if($guest_accommodations = $this->callTitanAPI('hotel/booking/getAccommodations/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token'])){
                TitanBasket::setAccommodations($guest_accommodations);
            }
        }

        //pre_r($guest_accommodations);exit;

        $pagination = $this->paginate($guest_accommodations,$all,$type.'s');

        //pre_r($guest_accommodations);exit;

        $data = array(
            'accommodations' => $this->getRecords($pagination['records'], $pagination['offset'], $pagination['per_page']),
            'type' => $type,
            'all' => $all
        );

        $this->__view('accommodations',$data);
    }

    public function credit($status = null,$token = null){

        if(!$status || !$token){

            $payment_methods = $this->callTitanAPI('hotel/booking/getPaymentMethods/' . $this->loyalty_id . '/' . $this->current_lang);
            $credit_values = $this->callTitanAPI('hotel/booking/getCreditValues/' . $this->loyalty_id);

            $data = array(
                'bookingStatus' => array(),
                'paymentMethods' => $payment_methods,
                'creditValues' => $credit_values,
            );
        }
        else{
            //$this->user['points'] = 0;

            if($response['data'] = $this->callTitanAPI('hotel/booking/getCreditRequest/'.$this->loyalty_id.'/'.$token,array('user_token'=>$this->user_data['user_token']))){
                if(isset($response['data']['payed']) && !empty($response['data']['payed'])){
                    $status = 'pending';
                    $smile_face = 'meh';
                    $alert = 'warning';
    
                    if(isset($response['data']['added_credit']) && !empty($response['data']['added_credit'])){
                        $status = 'success';
                        $smile_face = 'smile';
                        $alert = 'success';
                    }
                }
                else{
                    $status = 'error';
                    $smile_face = 'frown';
                    $alert = 'danger';
                }
            }
            else{
                redirect(base_url($this->current_lang.'/loyaltySystem/credit'));
            }

            if(!isset($this->user['credit']) || empty($this->user['credit'])){
                $this->user['credit'] = 0;
            }
            if(!isset($this->user['points']) || empty($this->user['points'])){
                $this->user['points'] = 0;
            }

            $data = array(
                'bookingStatus' => array(),
                'status' => $status,
                'smile' => $smile_face,
                'token' => $token, 
                'alert' => $alert, 
                'credit' => $response['data']['value'],
                'points' => $this->user['points'],
                'credit_usable' => $this->user['credit']
            );

        }

        $this->__view('credit',$data);
    }

    public function buyCredit(){

        $response = array(
            'status' => 0
        );

        if($post = $this->ci->input->post()){

            if(isset($post['value'],$post['payment']) && !empty($post['value']) && !empty($post['payment'])){
                
                $request = array(
                    'value' => $post['value'],
                    'payment' => $post['payment'],
                    'user_token' => $this->user_data['user_token'],
                    'http_refferer' => base_url($this->current_lang.'/loyaltySystem/credit')
                );

                if($res = $this->callTitanAPI('hotel/wallet/storeCreditRequest/'.$this->loyalty_id.'/'.$this->current_lang,$request)){
                    $response['status'] = 1;
                    $response['href'] = $this->api_endpoint.'payments/externalPayments/prepare/'.$this->current_lang.'/'.$this->loyalty_id.'/'.$res['id'].'/'.$res['token'].'/'.$this->user_data['user_token'];
                }
            }
        }

        echo json_encode($response); 
    }


    private function remakeBills($bills){
        //pre_r($bills);exit;
        $response = array();

        if(!empty($bills)){
            foreach($bills as $key => $bill){
                $response[$key]['bill_id'] = $bill['billId'];
                $response[$key]['bill_number'] = $bill['billNumber'];
                $response[$key]['price'] = 0;
                $response[$key]['currency_code'] = '';

                //pre_r($bill['items']);exit;
                if(isset($bill['items']) && !empty($bill['items'])){
                    foreach($bill['items'] as $item_key => $item){
                        $response[$key]['items'][$item['code']]['name'] = $item['name'];
                        if(isset($response[$key]['items'][$item['code']]['count']) && $response[$key]['items'][$item['code']]['count'] > 0){
                            $response[$key]['items'][$item['code']]['count']++;
                        }
                        else{
                            $response[$key]['items'][$item['code']]['count'] = 1;
                        }

                        $response[$key]['items'][$item['code']]['unit_price'] = $item['priceWithVat'];
                        $response[$key]['items'][$item['code']]['currency_code'] = $response[$key]['currency_code'] = $item['currencyCode'];

                        if($item['priceWithVat'] > 0){
                            $response[$key]['price'] += $item['priceWithVat'];
                        }
                        else{
                            $response[$key]['price'] -= ($item['priceWithVat']*(-1));
                        }
                    }
                }
            }
        }

        //pre_r($response);exit;

        return $response;
    }


    public function myAccommodation($accommodation_id){

        if(!$accommodation = TitanBasket::getAccommodation($accommodation_id)){
            redirect(base_url($this->current_lang . '/loyaltySystem/accommodations'));
        }

        if(isset($accommodation['bill']) && !empty($accommodation['bill']) && TitanBasket::showBills($this->hotel_data)){
            $accommodation['bills'] = $this->callTitanAPI('hotel/booking/getBills/'.$this->loyalty_id.'/'.$this->current_lang.'/'.$this->user_data['user_token'],array_unique(array_column($accommodation['bill'],'id')));
            $accommodation['bills'] = $this->remakeBills($accommodation['bills']);
        }

        //pre_r($accommodation['bills']);exit;

        if(!$branch_hotel_data = TitanBasket::getBranchHotelData($accommodation['branch_name'])){
            $branch_hotel_data = $this->getBranchHotelData($accommodation['branch_name']);
        }

        //pre_r($accommodation);exit;

        $data = array(
            'accommodation' => $accommodation,
            'branch_hotel_data' => $branch_hotel_data,
            'showBills' => TitanBasket::showBills($this->hotel_data)
        );

        $this->__view('myAccommodation',$data);
    }

    public function handleUpgradeOffer($data){
        TitanBasket::storeUpgrade($data['reservationID'],$data['searchKey'],$data['offerHash']);

        echo json_encode(array(
            'basket' => TitanBasket::getBasketView($this->hotel_data,$data['reservationID']),
        ));
    }

    public function handleGetSearchOffers($data,$fromSession){
        if (isset($data['hotelID'], $data['token'], $data['reservationID']) && !empty($data['hotelID']) && !empty($data['token']) && !empty($data['reservationID'])) {

            if($fromSession){
                if(!$offers = TitanBasket::getOffersByReservationID($data['reservationID'])){
                   $offers = $this->offersCall($data['hotelID'],$data['token'],$data['reservationID']);
                }
            }
            else{
                $offers = $this->offersCall($data['hotelID'],$data['token'],$data['reservationID']);
            }

            if(!$reservation = TitanBasket::getUserReservation($data['reservationID'])){
                $reservation = $this->getReservationDataTitan($data['hotelID'],$data['reservationID']);
            }
        }

        $basket = TitanBasket::getBasketView($this->hotel_data,$data['reservationID']);

        if(!empty($offers) && !empty($reservation)){
            if ($upgradeOffers = $this->getUpgradeOffers($offers, $reservation['request'],$data['reservationID'])) {
                $response['status'] = 1;
                $response['basket'] = $basket;
                foreach ($upgradeOffers as $search_key => $upgradeOffer) {
                    if (!empty($upgradeOffer)) {
                        $response['offers'][$search_key] = $this->ci->template->getView('loyalty/'.$this->hotel_data['loyalty'].'/partials/upgradeOffers', array('reservationID'=>$data['reservationID'],'upgradeOffers' => $upgradeOffer, 'searchKey' => $search_key));
                    } else {
                        $response['offers'][$search_key] = 'empty';
                    }
                }
            }
        }

        if (empty($response) || !isset($response['status']) || $response['status'] !== 1) {
            $response = array(
                'status' => 0,
                'message' => 'Nie sú ku dispozícii žiadne izby pre upgrade!',
                'basket' => $basket
            );
        }

        echo json_encode($response);
    }

    private function getReservationDataTitan($hotelID,$reservationID){

        if($reservation = $this->callTravelDataAPI('booking/hotel/getReservationDataTitan/' . $hotelID . '/' . $reservationID)){
            TitanBasket::setUserReservation($reservationID,$reservation);
        }

        return $reservation;
    }

    public function handleUpgradeCampareModal($data){

        $search_key = 'search-'.($data['searchKey']+1);
        $offers = TitanBasket::getOffersByReservationID($data['reservationID']);
        $myOffers = isset($offers[$search_key]) ? $offers[$search_key] : array();
        $reservation = TitanBasket::getReservationStack($data['reservationID']);

        $data['basketInfo'] = $reservation['request']['rooms'][$data['searchKey']]['roomData'];
        $data['upgradeOffer'] = $this->findRoom($data['offerHash'],$myOffers);

        $view = $this->ci->template->getView('order/partials/modalCompare',$data);

        echo json_encode(array(
            'status' => 1,
            'view' => $view
        ));
    }

    public function handleGetUpsellInfo($data) {

        $response = array(
            'view'=>$this->ci->template->getView('loyalty/'.$this->hotel_data['loyalty'].'/partials/upsell',array('upsell' => TitanBasket::getUpsellCategory($data['reservation_id'],$data['category_id']),'upsell_id'=>$data['upsell_id'])),
        );

        echo json_encode($response);
    }



    public function handleRemoveDataFromBasket($data){
        switch($data['type']){
            case 'upsells':
                TitanBasket::removeUpsell($data['reservation_id'],$data['search_key'],$data['upsell_id']);
                break;
            case 'hosts':
                TitanBasket::removeHostData($data['reservation_id'],$data['search_key']);
                break;
            case 'upgrades':
                TitanBasket::removeUpgrade($data['reservation_id'],$data['search_key']);
                break;
        }


        echo json_encode(array(
            'basket' => TitanBasket::getBasketView($this->hotel_data,$data['reservation_id']),
        ));
    }

    public function handleStoreGuestData($data,$reservationID,$searchKey){
        $status = 0;
        $view = 0;

        $hostData = array();

        if (!empty($data)) {
            foreach ($data as $field => $d) {
                if (!empty($d)) {
                    foreach ($d as $host_key => $val) {
                        if ($val === '') {
                            $status = 0;
                            break 2;
                        }

                        $hostData[$host_key][$field] = $val;
                        $status                      = 1;
                    }
                }
            }
        }

        if ($status === 1) {
            TitanBasket::storeHostData($reservationID, $searchKey, $hostData);
            $view = TitanBasket::getBasketView($this->hotel_data,$reservationID);
        }

        echo json_encode(array(
            'status' => $status,
            'basket' => $view
        ));

        exit;
    }

    public function handleRemoveUpsell($data){
        $status = 0;
        $view = null;

        if ($upsell = TitanBasket::getUpsell($data['data']['reservation_id'], $data['data']['category_id'], $data['data']['upsell_id'])) {
            TitanBasket::removeUpsell($data['data']['reservation_id'], $data['data']['room_key'], $data['data']['upsell_id']);
            $view = TitanBasket::getBasketView($this->hotel_data,$data['data']['reservation_id']);
            $status = 1;
        }

        $response = array(
            'status' => $status,
            'basket' => $view
        );

        echo json_encode($response);
    }

    public function handleStoreUpsell($data){
        $status = 0;
        $view = null;

        if ($upsell = TitanBasket::getUpsell($data['data']['reservation_id'], $data['data']['category_id'], $data['data']['upsell_id'])) {

            $basketPoints = TitanBasket::getUpsellsPoints($data['data']['reservation_id']);

            if (($basketPoints + ($upsell['points'] * $data['count'])) < $this->user['points']) {
                $upsellData = array(
                    'count' => $data['count'],
                    'price' => $upsell['price'],
                    'points' => $upsell['points'],
                    'category_id' => $data['data']['category_id'],
                    'upsell_id' => $data['data']['upsell_id'],
                );

                TitanBasket::storeUpsell($data['data']['reservation_id'], $data['data']['room_key'], $data['data']['upsell_id'], $upsellData);

                $status = 1;
            }
        }

        $response = array(
            'status' => $status,
            'basket' => TitanBasket::getBasketView($this->hotel_data,$data['data']['reservation_id'])
        );

        echo json_encode($response);
    }

    public function editBooking($reservationID)
    {
        $response['status'] = 0;

        $this->ci->curl->create(TitanBasket::getEditBookingEndPoint($this->web_service_endpoint,$reservationID));
        $this->ci->curl->http_header('API-KEY', $this->apiKey);
        $this->ci->curl->post(TitanBasket::getBasket($reservationID));
        $bookingResponse = json_decode($this->ci->curl->execute(), TRUE);

        if (isset($bookingResponse['status'],$bookingResponse['data']) && !empty($bookingResponse['data'])) {
            TitanBasket::cleanBasket($reservationID);
            $response['status'] = 1;
        }

        echo json_encode($response);
    }

    private function setCookie($response){
        $this->ci->load->helper('cookie');

        $user_id_cookie = array(
            'name' => 'gps_user_id',
            'value' => $response['user_id'],
            'expire' => '315360000',  // 10 rokov
        );
        $token_cookie   = array(
            'name' => 'gps_token',
            'value' => $response['user_token'],
            'expire' => '315360000',  // 10 rokov
        );

        $remember_cookie = array(
            'name' => 'gps_remember',
            'value' => 1,
            'expire' => '315360000',  // 10 rokov
        );

        set_cookie($user_id_cookie);
        set_cookie($token_cookie);
        set_cookie($remember_cookie);
    }

    private function loadData(){

       
        if (isset($_COOKIE['gps_remember']) && $_COOKIE['gps_remember'] === '1') {
           
            if (isset($_COOKIE['gps_user_id'],$_COOKIE['gps_token'])) {
                $this->user_data = array(
                    'user_token' => $_COOKIE['gps_token'],
                    'user_id' => $_COOKIE['gps_user_id'],
                );

                TitanBasket::setUserData($this->user_data);
            }
        }

    }

    private function offersCall($hotelID,$token,$reservationID){

        if($offers = $this->callTravelDataAPI('booking/Hotel/getOffers/' . $hotelID . '/' . $token,array())){
            TitanBasket::storeOffers($reservationID,$offers);
        }

        TitanBasket::storeOffers($reservationID,$offers);

        return $offers;
    }

    private function getBranchHotelData($branch_name){


        if($branch_hotel_data = $this->callTravelDataAPI('booking/hotel/getInfoByBranchName/' . $this->current_lang,array('branch_name' => $branch_name))){
            TitanBasket::setBranchHotelData($branch_name,$branch_hotel_data);
            return $branch_hotel_data;
        }

        return array();
    }

    private function getRecords($records, $offset, $per_page){
        $response = array();

        $counter = 0;
        if (!empty($records)) {
            if ($offset != null) {
                foreach ($records as $key => $record) {
                    if ($counter == $offset) {
                        $response[$key] = $record;
                    } elseif ($counter > $offset - 1 && $counter < $per_page + $offset) {
                        $response[$key] = $record;
                    }
                    $counter++;
                }
            } else {
                foreach ($records as $key => $record) {
                    if ($counter < $per_page) {
                        $response[$key] = $record;
                    } else {
                        break;
                    }
                    $counter++;
                }
            }
        }

        return $response;
    }

    private function paginate($records,$all,$type,$check = true){
        $response = array();

        $this->ci->load->library('pagination');

        $response['records'] = array();
        $response['count'] = count($records);
        $response['per_page'] = 5;
        $response['page']  = base_url($this->current_lang.'/loyaltySystem/'.$type.'/');
        $response['segment'] = 5;

        //pre_r($type);exit;

        if ($all === 'all') {
            $response['page'] .= 'all/';
            $response['records'] = $records;
        } 
        else {
            if($check){
                if($type === 'accommodations'){
                    foreach($records as $key => $record){
                        //pre_r($record);exit;
                        if (strtotime(date('Y-m-d H:i:s',strtotime($record['date_to']))) > strtotime(date('Y-m-d H:i:s'))) {
                            $response['records'][$key] = $record;
                        }
                    }
                }
                else{
                    foreach($records as $key => $record){
                        foreach($record as $key_2 => $rec){
                            if (strtotime(date('Y-m-d H:i:s',strtotime($rec['date_to']))) > strtotime(date('Y-m-d H:i:s'))) {
                                $response['records'][$key][$key_2] = $rec;
                            }
                        }
                    }
                }
            }
            else{
                foreach($records as $key => $record){
                    $response['records'][$key] = $record;
                }
            }


            $response['count']   = count($response['records']);
            $response['segment'] = 4;
        }

        //pre_r($response);exit;

        $config = array(
            'base_url' => $response['page'],
            'total_rows' => $response['count'],
            'per_page' => $response['per_page'],

            'full_tag_open' => '<div id="pagination">',
            'full_tag_close' => '</div>',

            'first_link' => '<<',
            'last_link' => '>>',

            'first_tag_open' => '<span class="first">',
            'first_tag_close' => '</span>',

            'last_tag_open' => '<span class="last">',
            'last_tag_close' => '</span>',


            'next_tag_open' => '<span class="next">',
            'next_tag_close' => '</span>',

            'prev_tag_open' => '<span class="prev">',
            'prev_tag_close' => '</span>',

            'display_pages' => TRUE,
        );

        $this->ci->pagination->initialize($config);
        $response['offset'] = $this->ci->uri->segment($response['segment']);

        return $response;
    }

    private function removeData(){
        TitanBasket::cleanData();
        $this->user = null;
        $this->getUserData();
    }

    private function getUpgradeOffers($offers, $reservation,$reservationID){
        $upgradeOffers = array();


        $booking = TitanBasket::getBooking($reservationID);
        if (isset($reservation['rooms']) && !empty($reservation['rooms'])) {
            foreach ($reservation['rooms'] as $roomKey => $room) {
                if (!isset($room['decoded_offer_hash']) || empty($room['decoded_offer_hash'])) continue;
                if(isset($booking['bookingRooms'][$roomKey]['roomType']) && !empty($booking['bookingRooms'][$roomKey]['roomType']) && $booking['bookingRooms'][$roomKey]['roomType'] == $room['decoded_offer_hash']['roomTypeCode']){
                    if (isset($offers) && !empty($offers)) {
                        foreach ($offers as $search_key => $offers_by_search_key) {
                            if($upgrade = TitanBasket::getUpgrade($reservationID,$roomKey)){
                                $upgradeOffer = $this->findRoom($upgrade,$offers_by_search_key);
                            }
                            elseif ($room['searchKey'] !== $search_key) {
                                continue;
                            }
                            if (isset($offers_by_search_key) && !empty($offers_by_search_key)) {
                                foreach ($offers_by_search_key as $offer) {
                                    if (isset($offer['offers']) && !empty($offer['offers'])) {
                                        foreach ($offer['offers'] as $key => $item) {
                                            if ($upgrade) {
                                                if ($upgradeOffer['flexiType'] == $item['flexiType'] && $item['offerType'] === $upgradeOffer['offerType'] &&
                                                    $upgradeOffer['flexiID'] == $item['flexiID'] && $item['offerID'] == $upgradeOffer['offerID'] && $upgradeOffer['price'] < $item['price']) {
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]                       = $item;
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['priceDiff']          = $item['price'] - $room['price'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomName']           = $offer['roomName'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomDescription']    = $offer['roomDescription'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['images']             = $offer['images'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['currency']           = $item['currency'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomCategoryID']     = $offer['roomCategoryID'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['terminData']         = $offer['terminData'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomFacilities']     = $offer['roomFacilities'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomAdditionalInfo'] = $offer['roomAdditionalInfo'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomID']             = $offer['roomCategoryID'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['searchKey']          = $room['searchKey'];
                                                }
                                            } else {
                                                if ($room['decoded_offer_hash']['flexiType'] == $item['flexiType'] && $item['offerType'] === $room['decoded_offer_hash']['offerType'] &&
                                                    $room['decoded_offer_hash']['flexiID'] == $item['flexiID'] && $item['offerID'] == $room['decoded_offer_hash']['offerID'] && $room['price'] < $item['price']) {
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]                       = $item;
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['priceDiff']          = $item['price'] - $room['price'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomName']           = $offer['roomName'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomDescription']    = $offer['roomDescription'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['images']             = $offer['images'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['currency']           = $item['currency'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomCategoryID']     = $offer['roomCategoryID'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['terminData']         = $offer['terminData'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomFacilities']     = $offer['roomFacilities'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomAdditionalInfo'] = $offer['roomAdditionalInfo'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['roomID']             = $offer['roomCategoryID'];
                                                    $upgradeOffers[$roomKey][$offer['roomCategoryID']][$key]['searchKey']          = $room['searchKey'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    break;
                }
            }
        }

        if (!empty($upgradeOffers)) {
            return $upgradeOffers;
        }
        return false;
    }

    private function findRoom($offerHash,$offers){
        $room = array();

        if(!empty($offers)){
            foreach($offers as $offer){
                if(!empty($offer['offers']) && !empty($offer['offers'])){
                    foreach($offer['offers'] as $offerItem){
                        if(htmlspecialchars($offerItem['offerHash']) == htmlspecialchars($offerHash)){
                            $room = $offer;
                            unset($room['offers']);
                            $room = array_merge($room,$offerItem);
                        }
                    }
                }
            }
        }

        return $room;
    }

    public function validateCard($post = array()){

        $response = array(
            'status' => 0,
            'message' => 'Užívateľ sa nenašiel'
        );

        if(!empty($post)){
            
            if($users = $this->callTitanAPI('hotel/users/getUserByValue/'.$this->loyalty_id.'/1/1',array( 'type' => 'card' , 'value' => $post['card'] ))){

                foreach($users as $user){
                    if(slugify($user['surname']) == slugify($post['last_name'])){
                        $response = array(
                            'status' => 1,
                            'action' => base_url($this->current_lang.'/loyaltySystem/register'),
                            'user_data' => array(
                                'name' => $user['name'],
                                'surname' => $user['surname'],
                                'street' => isset($user['addrress']['street']['name']) && !empty($user['addrress']['street']['name']) ? $user['addrress']['street']['name'] : null,
                                'city' => isset($user['address']['city']['name']) ? $user['address']['city']['name'] : null,
                                'zip' => isset($user['address']['postcode']) ? $user['address']['postcode'] : null,
                                'email' => isset($user['email']) && !empty($user['email']) ? $user['email'] : null,
                                'phone' => isset($user['telephoneNumber']) && !empty($user['telephoneNumber']) ? $user['telephoneNumber'] : null,
                                'card' => $post['card']
                            )
                        );
                        break;
                    }
                }
            }

        }

        echo json_encode($response);

    }

    private function trinityPreregistration($user_data,$data){

        if($user = $this->callTitanAPI('hotel/users/createUser/'.$this->loyalty_id.'/'.$this->current_lang,TitanBasket::getCreateUserDataTrinity(array_merge($user_data,$data)))){
            $this->setCookie($user);
            $this->user_data = $user;
            TitanBasket::setUserData($this->user_data);

            if ($guest = $this->getUserData()) {
                echo json_encode(array('status' => 1, 'guest' => $guest, 'layout' => $this->ci->template->getView('layout/partials/' . $this->theme_config['version'] . '/guest/logged', array('user' => $guest))));
                exit();
            }
        }
    }

    public function specialOffers(){
        $user_data = TitanBasket::getUserData();

        if(!$special_offers = TitanBasket::getSpecialOffers()){
            if($special_offers = $this->callTravelDataAPI('booking/hotel/getSpecialOffers/' . $this->hotelID . '/' . $this->current_lang.'/'.$user_data['user_token'])){
                TitanBasket::setSpecialOffers($special_offers);
            }
        }

        $data = array(
            'specialOffers' => $special_offers,
            'type' => 'booking'
        );

        $this->__view('specialOffers',$data);

    }

    private function __view($view,$data = array()){

        $response = array(
            'hotelID' => $this->theme_config['hotelID'],
            'hotel' => $this->hotel_data,
            'currentLang' => $this->current_lang,
            'registeredGuestData' => TitanBasket::getUser(true)
        );

        $this->ci->template->set('title', $this->hotel_data['hotelName']);
        $this->ci->template->view('loyalty/'.$this->hotel_data['loyalty'].'/'.$view, array_merge($response,$data));
    }

    private function callTitanAPI($method,$data = null){
        //pre_r(json_encode($data));exit;
        //pre_r($this->api_endpoint . $method);exit;
        $this->ci->curl->create($this->api_endpoint . $method);
        $this->ci->curl->http_header('API-KEY', $this->api_key);
        if($data){
            $this->ci->curl->post($data);
        }

        $response = json_decode($this->ci->curl->execute(),true);

        if(isset($response['status'],$response['data']) && !empty($response['data'])){
            return $response['data'];
        }

        if(isset($response['message'])){
            $this->messages = $response['message'];
        }

        return array();
    }

    private function callTravelDataAPI($method,$data = null){
        $this->ci->curl->create($this->web_service_endpoint . '/' . $method);
        $this->ci->curl->http_header('API-KEY', $this->apiKey);
        if($data){
            $this->ci->curl->post($data);
        }
        $response = json_decode($this->ci->curl->execute(),true);

        if(isset($response['status'],$response['data']) && !empty($response['data'])){
            return $response['data'];
        }

        if(isset($response['message']) && !empty($response['message'])){
            $this->messages = $response['message'];
        }

        return array();
    }

}