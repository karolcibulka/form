<?php
(defined('BASEPATH')) or exit('No direct script access allowed');


class MY_Controller extends CI_Controller {


    public $upload_dir;
    public $image_size;

    public function __construct()
    {

       parent::__construct();

        $lang_file_names = array(
            'sk' => 'slovak',
        );

        $this->lang->load(array('default'));

        $this->load->config('theme');
        $this->theme_config = $this->config->item('theme');
        $this->current_lang = $this->uri->segment(1);
        $this->upload_dir = 'upload/images/';
        $this->image_size = 200;


        $this->template->set('current_lang',$this->current_lang);

        if (isset($this->theme_config['version']) && ($this->theme_config['version'] != 'default')){
            $this->template->setTheme($this->theme_config['version']);
        }
    }

}
