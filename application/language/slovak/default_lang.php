<?php

$lang['content.first_name'] = 'Meno';
$lang['content.last_name'] = 'Priezvisko';
$lang['content.email'] = 'Email';
$lang['content.password'] = 'Heslo';
$lang['content.password_confirmation'] = 'Potvrdenie hesla';
$lang['content.send_button'] = 'Odoslať';
$lang['content.registration_form'] = 'Registračný formulár';
$lang['content.profile_image'] = 'Profilový obrázok';

$lang['validation.email.exist'] = 'Email s adresou <strong>%s</strong> existuje';
$lang['validation.password.match'] = 'Zadané heslá sa nezhodujú';
$lang['validation.password.length'] = 'Zadané heslo je príliš krátke, použite <strong>najmenej 8 znakov</strong>';

$lang['required.first_name'] = 'Meno je povinné pole';
$lang['required.last_name'] = 'Priezvisko je povinné pole';
$lang['required.email'] = 'Email je povinné pole';
$lang['required.password'] = 'Heslo je povinné pole';
$lang['required.password_confirmation'] = 'Heslo znova je povinné pole';

$lang['registration.success'] = 'Vaša registrácie prebehla v poriadku, teraz mi môžete dať další task';