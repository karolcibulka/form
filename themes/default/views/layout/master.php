<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Registráčný formulár</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>

    <style>
        .card,
        .form-control,
        .btn{
            border-radius:0;
        }

        body,
        input.form-control,
        .btn{
            font-size:12px;
            font-weight:300;
        }

        .w-100{
            width: 100%;
        }

        .fw-300{
            font-weight: 300;
        }

        body{
            background-color:#007cfe;
        }

        .h-100vh{
            height:100vh;
        }

        .mh-100{
            min-height:100%;
        }
    </style>
</head>
<body>
    
    <?=$content?>

</body>
</html>